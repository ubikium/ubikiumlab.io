---
title: Setting up an Esperanto keyboard layout on Windows
author: ubikium
date: 2020-07-18
---

## Problem

Esperanto is an easy to learn language, however, it lacks major support from locale providers. Windows 10 has an Esperanto locale in its language setting, but the locale doesn't have any actual component (or a [language pack](https://support.microsoft.com/en-us/help/14236/windows-language-packs) in the Windows parlance).

One crucial missing part is the ability to type diacritic letters, `ŝĝĵĥĉŭ`, with the keyboard. Of course you can install an additional input method software, or use a language that already has these letters. But actually it can be done more easily. This post will teach you how.

## Method

A keyboard layout is a mapping from keys to characters (or control sequences). We can use a special layout to map some keys to the Unicode characters of special letters (e.g. `q` → `ŝ`). The steps are:

### 1. Install the keyboard layout

If you're using an "United States - English" keyboard and **aren't using the "English (Canada)" locale**, you can download the layout from [gellertb97/eo_US-Windows-Keyboard](https://github.com/gellertb97/eo_US-Windows-Keyboard/). I'll talk about the case of other keyboards in [the FAQ section](#faq).

![Esperanto Keyboard Layout](https://raw.githubusercontent.com/gellertb97/eo_US-Windows-Keyboard/master/res/eo_US.jpg)

You can learn the mapping from `README.md`. Follow the instructions in the `Install` folder, and you can add the keyboard layout to the "English (Canada)" or `en_CA` locale.

You might need to restart your computer for the keyboard to be available.

You can stop at this point if you want. The problem is that if you have another English keyboard layout, they will both be displayed as an `ENG` language icon. So you can't tell which one you are using. We solve this by moving the keyboard to the Esperanto locale.

### 2. Move the layout to the Esperanto locale

`Start` > `Settings` > `Time & Language` > `Language` > `Add a language`, then search for Esperanto.

Back to `Language` and select `Esperanto` > `Options` > `Add a keyboard`. Select `Esperanto (United States/Canada)`.

### 3. Clean the "English (Canada)" locale

In `Language`, select `English (Canada)` and delete it.

Now you're done. Use `Alt+Shift` (or `Win+Space`) to switch keyboards. You should see the new layout under the language Esperanto (the icon is `EPO`).

Enjoy🎉!

## Customization

As is explained in the `README.md`, the layout is created with the [Microsoft Keyboard Layout Creator](https://www.microsoft.com/en-us/download/details.aspx?id=22339) (MKLC). The layout source file is available at `src/eo_US.klc`. You can install MKLC and open the `klc` file for further customization.

When done configuring, use `Project` > `Build DLL and Setup Package` to validate and build the layout. The setup package will be built under your `Current working directory` folder. You can install it or distribute it.

Sometimes, MLKC will complain that the keyboard is already in use on this machine. Or when installing, it says the `eo_US.dll` is already in the system folder. You need to first uninstall the keyboard and the DLL. Then build and install again. See the [FAQ section](#faq).

## FAQ

### Why not install to the Esperanto locale in the first place

The [MKLC tool](#customization) doesn't have Esperanto as an available locale option. So we have to first install the keyboard under a different language and then move it to Esperanto.

### I already use the "English (Canada)" locale and cannot delete the keyboard

If you have already added the keyboard to "English (Canada)". The custom keyboard's `Delete` option might be grayed out. See below on how to uninstall the keyboard while keeping the locale.

Alternatively, you can use the MKLC tool to select an unused language locale (e.g. "Latin (World)"). Build the setup package and install again.

### I want to type English letters at the same time (Multilingual)

If you want to type both English and Esperanto letters without switching keyboards, you need to create a multilingual layout.

In the MKLC tool, you can assign the Esperanto letters to other keys (potentially in combination with modification keys, `Shift`, `Alt+Ctrl`, and `Ctrl`). (e.g. `Alt+Ctrl+s`→`ŝ`)

An implementation of the `Alt+Ctrl` solution for the "United States - English" keyboard can be found in the same author's [another repository](https://github.com/gellertb97/eo_INTL-Windows-Keyboard).

### Can't install/Can't build DLL/How to uninstall

Sometimes, MLKC will complain that the keyboard is already in use on this machine. Or when installing, it says the `eo_US.dll` is already in the system folder. You need to first uninstall the keyboard and the DLL. Then build and install again.

The setup package, unfortunately, doesn't come with an uninstallation program. The following is my best effort:

1. If you installed the keyboard under an unused locale, delete the locale.
2. Purge the keyboard's registry entry.
    - **Backup your registry file**.
    - With `Regedit`, navigate to `HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\Keyboard Layouts\`. You should find a list of layouts. The custom layout should have a large key number. For me, the normal layouts begin with `00`, and the custom layout begins with `a0`. Make sure that the `Layout Text` is `Esperanto (United States/Canada)` and the `Layout File` is `eo_US.dll` (or other values in accordance with your assignments).
    - Delete the custom layout entry.
3. Delete the DLL file.
    - Find `eo_US.dll` file. The DLL file usually lives in `C:\\Windows\System32`. If you can't find it, search through `System32`'s subfolders.
    - You might want to backup it first.
    - Delete it (this usually needs administrator privileges).

You might need to restart your computer to complete the uninstallation.

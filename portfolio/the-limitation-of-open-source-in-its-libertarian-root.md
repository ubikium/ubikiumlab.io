---
title: The Limitation of Open Source in its Libertarian Root
author: ubikium
date: 2024-05-05
---

Or how people learned to start worrying and fear the bomb.

A recent controversy brought up some deep questions about the nature of free & open source software movement (referred to as open source in this article).
The details are not important for our purpose.
Let's just say there's an open source community.
At a community event, people discovered that a military contractor company is on the sponsor list.
A part of the community voiced their concerns and some decided to drop this technology, both in their professional use and personal use.

At first sight, both of reactions (rejecting sponsorship and dropping usage) seem unreasonable.

Rejecting the military contractor company's contribution or sponsorship doesn't really prevent it from accessing the tool, because the source code is publicly available.
It could be argued that even if one doesn't agree with them, accepting the sponsorship is not worse than before, where they simply take away the benefits without anyone knowing.

Furthermore, because the tool is general purpose, there's also a technological neutrality angle here.
It would cause much more damage to the military-industry complex if they cannot use Microsoft Word, but there are very few people advocating a boycott of Microsoft for this.

On the other hand, it could be argued that accepting their sponsorship is a form of association and endorsement, where we are normalizing their unagreeable behaviors and saying that it's okay to develop the tool using "blood money".
However, given that the person has decided to exit, the "rational" thing to do is to keep a private fork and use the tool secretly.
Dropping even the personal usage seems more like a moral statement, in the realm of magical thinking, where everything touched by the bad guys turns bad as well.

In this vein, some think that it's unnecessary to react to the problem in this way, because the problem is not that the situation has gone worse, rather it's being reminded that previously it's bad.
Such reactions are like killing the messenger bearing the bad news.

All above points are not wrong, but nevertheless some genuine concerns are expressed in those actions, because whether or not the messenger is killed, the bad news is already there.
At least killing the messenger is a form of acknowledging the bad situation.
It is better than keeping the messenger alive, while turning a deaf ear to them as some have suggested.

The deep question is about the nature of open source.
It often lingers in the background, but no discussion seems to be able to circumvent referring back to this question.
In lieu of how the professor in *The Moon is a Harsh Mistress* reduced a person's entire political philosophy into an answer to a single question, I'll try to do the same here.
People's understanding of the nature of open source can be roughly determined by their answers to this question:

**Does freedom for all extend to the bad guys?**

In open source, participants with different answers can and often do work together.
However, when crisis comes, the difference emerges and people are surprised to learn about answers different from their own.

The rest of the blog post argues for the following points:

1. The founding principles of open source answered "yes" to this question due to its libertarian leaning, but didn't explicitly state such.
2. This flexibility accounts for part of the movement's success, because it allows the movement to carry on even if the practice has forced some deviation from this answer, namely some participants began to answer "no" to the above question.
3. The reason for this deviation is due to the libertarian focus on the personal sphere of freedom is superseded by the societal level of freedom.
4. It remains to be seen whether an alternative movement will emerge and become a substituting power.

## The libertarian view: all means all

The founding principles of open source answered the above question with a resounding "yes".
Freedom for all, even for the bad guys.
To the potential objections of their answer, a libertarian would say:

- It's not for us to judge who are the bad guys, and
- The problem should be addressed on the societal level (how the tools are used), rather than the personal level (who has access to the tools). It's both unreasonable and futile to regulate any unagreeable usage.

Apart from the technological neutrality view, the answer sets a stark contrast between the person and the society along the lines of libertarian thought.
On one hand, the person enjoys the maximum freedom as the society would allow.
On the other hand, the society enjoys the maximum freedom as the person would grant.
Therefore, if a person doesn't judge a practice to be bad, the society is free to exercise it as it pleases.
Only those who judge it to be bad should act to stop it.

## The deviation in practice

Nowadays, open source participants usually don't agree that freedom for all should extend to the bad guys.
This change is mainly due to software and its creators becoming more integral to the society.
As such, the libertarian assumptions start to break down.

"It's not for us to judge who are the bad guys."
Previously "us" only includes the people who created the software, and usually excludes people who are affected by the software.
However, when the creators have a stronger connection to those who are affected, or in some cases, they are the same people, they do feel the obligation to judge who are the bad guys.
An example would be if a corporation asks the employee to develop a software for the surveillance of the employee themselves.

The technological neutrality part remains largely true for general tools.
However, if an unagreeable usage requires continuous effort to create a certain tool, the participants can create some barriers simply by collectively choosing to not contribute to it, or to contribute to an adversarial tool.
For example, more public effort has been spent on ad-blockers than advertising tools.
As a result, advertising companies have to actively invest in such tools and compete with each other, combating with widely available ad-blockers at the same time.

## A conflict from ambiguity and its ambiguous result

The conflict from the beginning of the article can be viewed as people who participated the community under the assumption that the answer is "no", but surprisingly found out that the theoretical "yes" answer was invoked in practice.
The problem is not that these people "didn't read the fine print", but that the practice has evolved beyond those rules.
In other words, if people don't think there are ways to prevent bad guys from benefiting from their work, they wouldn't contribute in the first place.
The libertarian "openness to all" translates to "poor children in third world" in people's imagination and that's what drives open source forward.
It's like the Zizek comment: a community's unwritten rules are the most important rules.
Thus, it's no surprise that invoking the "technically correct" answer will burn people's good will, because it gives people a sense of betrayal—a real sense of real betrayal.

For now, the deviation has yet to materialize into a definite form.
There are efforts to create alternative licenses or add "Code of Conduct" as a community moderation guideline.
However, as long as the new ideas are embedded in the old open source movement, they will always be limited by its libertarian root.
Until then, not taking actions against bad guys can always be shrouded in the name of openness and inclusion.
The most people can do is to hard-fork the project and thus the community and hope to nurture those unwritten rules again through a different set of ethos.
From the libertarian view, this is hailed as exercising the freedom to exit, but in practice, it is often detrimental to the development of the tool.

People have yet to find a way to control the usage of the tools created by them, especially when the source code is open.


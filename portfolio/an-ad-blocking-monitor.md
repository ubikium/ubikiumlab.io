---
title: 'Rainsea Radio: An AD Blocking Monitor'
author: ubikium
date: 2023-09-10
---

Today's Rainsea Radio interview features the creator of a controversial startup, ClearUrGlass.
People have described that the startup's business is to destroy all other tech companies' business, as its product, the CUG-1 smart monitor, automatically detects and remove ads from screen.

The creator, a young and energetic Estonian man named Purigian Vitrokov disagrees.
"Being able to see clearly is a powerful value position for all the parties in the tech industry, especially now."
We'll dive deeper into his story in our interview.

```
[ads removed by ClearUrGlass]
```

(RR for Rainsea Radio, PV for Purigian Vitrokov)

> RR: Why did you create this product?

PV: I saw an ad from an activity group about this [Web Environment Integrity issue](https://www.fsf.org/blogs/community/web-environment-integrity-is-an-all-out-attack-on-the-free-internet) and realized that, we are really moving towards a society where ads are no longer avoidable.
Basically the website will be able to detect whether you're using an ad blocker and then decide whether to serve you the content.
We no longer own our browser.

But then I thought, wait a minute, it's nothing new!
There are, for example, bank apps, that detect whether your phone is rooted and if so, refuse to run.
Not to mention Apple's control on the hardware and the OS.
So if you really think about it, when it comes to tech stuff, we own nothing.
We are only tenants to our devices, our OS, and all the applications on top of that.
And the rent is paid dearly, because we let the owners control our thought.

Of course, nowadays, it's not a big deal.
Everyone knows and accepts it as a fact of life.
People think that there's nothing we can do.
But I went a step further.
I began to ask: what is the thing that we can actually own?
It takes a bit of, as they say, thinking outside of the box, because we already lost whatever is in the tower box.

So I came up with some ideas: a mouse that doesn't click ads, a keyboard that doesn't allow you to input your credit card number if you are purchasing through an ad, a headphone that doesn't play sound for ads, and finally, a monitor that doesn't display ads.
Then I took these ideas to a friend of mine, Ivan Hackerjev, a tech worker.
He decided that the monitor thing is the best option, so we started off.

The development cost is paid off by selling my NFTs before they all crashed.
And now we have this beautiful monitor, the CUG-1 monitor, that removes all the ads from your screen.

> RR: The RR team has tried out the monitor and we all agree that it enhanced our browsing experience a lot.
> It almost remined us of a time when browser based ad blocking was legal.
> What's the technology behind this monitor?

PV: The gist of the idea is that after the content of the screen is produced, a small program will run over the framebuffer, and purge the things that it deems as ads.
The detection algorithm uses AI and has some overhead, but the feedbacks so far suggested that ads are often more obnoxious and users feel that the rendering speed is improved because fewer things are shown on the screen.

```
[ads removed by ClearUrGlass]
```

> RR: What do you think is in the future of this product?

PV: On the product side, the short term goal is to provide some content where the ads are removed.
One of the downside of ad blocking is that there are so much space between contents now and users often feel boring while scrolling to the next block of contents.
I'm thinking maybe we can replace those voids with poems or cartoons to help people enjoy scrolling through those gaps more.

In the long term, I see a lot of potential in hardware-based ad blocking technologies.
Ads are not only displayed through a monitor, so we are thinking about applying the same tech to create a new kind of glasses that block ads.
This will also remove the limitations of the CUG-1 monitor, since it can't block ads on laptops and billboards.

In a way, this is the natural next step, as it's already in our company's name.


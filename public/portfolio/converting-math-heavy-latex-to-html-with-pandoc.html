<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="author" content="ubikium" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Cache-Control" content="max-age=86400, must-revalidate" />
        <title>Converting math-heavy LaTeX to HTML with Pandoc</title>
        <link rel="stylesheet" type="text/css" title="hakyll_theme" href="../css/theprofessional.css" />
        <link rel="stylesheet" type="text/css" title-"syntax_highlighting" href="../css/monokai.css">
        <script src="../assets/highlight.min.js"></script>
        <script type="text/javascript" charset="UTF-8" src="../assets/lean.min.js"></script>
        <script type="text/javascript">hljs.highlightAll();</script>
    </head>
    <body>
      <div class="highbar">&nbsp;</div>
        <div id="header">
          <div class="box">
            <div id="logo" class="name">
              <h2><a href="../">Ubikium</a></h2>
            </div>
            <div id="navigation" class="pageslinks">
              <nav class="menuNav">
                <div class="menuItems">
                <a href="../" class="portfolio/converting-math-heavy-latex-to-html-with-pandoc.md">Home</a>
                <a href="../about.html" class="portfolio/converting-math-heavy-latex-to-html-with-pandoc.md">About</a>
                <a href="../archive.html" class="portfolio/converting-math-heavy-latex-to-html-with-pandoc.md">Portfolio</a>
                </div>
              </nav>
            </div>
        </div>
        </div>
        <div class="container-gallery">
        <div id="content" class="inside">
            <div class="info">
    
      <h1>Converting math-heavy LaTeX to HTML with Pandoc</h1>
    
    Posted on 2024-12-20
    
        by ubikium
    
</div>
<hr>

<p>Recently there’s a need to convert a math-heavy LaTeX document to HTML.
Of all the tools I tried, <a href="https://pandoc.org/">Pandoc</a> wins out as it’s the only one that didn’t choke on macros.
It generated a surprisingly decent output and offered great customizability.
In this post I’ll talk about the general setup and some tips on how to extend the process to add various features.</p>
<p>For a complete example, see <a href="https://github.com/yuxiliu-arm/herdtools7/tree/110e001c748379ce2325d8523e4357e061894125/asllib/doc">this directory</a>.
It features a conversion from a document with various kinds of math packages into a website with interlinked pages.
Everything should follow from the <code>run-pandoc.sh</code> script.</p>
<h2 id="general-setup-and-limitations">General setup and limitations</h2>
<p>The overall pipeline is:</p>
<pre><code>LaTeX source
  ↓ pandoc reader
pandoc AST
  ↓ filters (0 or more)
pandoc AST
  ↓ pandoc writer
HTML files
  ↓ MathJax rendering
rendered HTML files</code></pre>
<p>To achieve the desired end goal for a specific component, you’ll need to think about at which step this transformation should happen and how it would affect other things in the pipeline.
For example, to rewrite links to a certain format, you can do that with a custom reader, or a Lua filter, or by JavaScript in the final HTML file.</p>
<p>What kind of LaTeX sources are suitable for this pipeline?
Pandoc can recognize common packages and their commands.
Of course there’s a limitation, which roughly corresponds to what’s expressible in <a href="https://pandoc.org/chunkedhtml-demo/8-pandocs-markdown.html">Pandoc’s Markdown</a>.</p>
<p>As for your own macros, Pandoc can parse and perform macro expansion inside the reader.
So basically if you are using macros for simple string substitution, rather than general programming, there’s a very good chance that it just works.
You can also use this as a way to redefine macros not supported by Pandoc (or MathJax).</p>
<h2 id="preserve-math-blocks">Preserve math blocks</h2>
<p>You’ll probably want to use <a href="https://www.mathjax.org/">MathJax</a> to render your math.
To do that, you’ll need to use <code>pandoc --standalone --mathjax</code>.
<code>--standalone</code> instructs Pandoc to generate the headers and footers, etc.
Otherwise, Pandoc assumes it’s transforming a document fragment and thus will not add those components.
The <code>--mathjax</code> option will prevent Pandoc from rendering the math blocks itself, but preserve the LaTeX commands and add a segment in the header to load the MathJax module, which will render the LaTeX commands into actual things that can be displayed by the browser.</p>
<p>To customize <a href="https://docs.mathjax.org/en/latest/options/index.html">MathJax options</a>, add a custom header with the <code>--include-in-header header.html</code> option.</p>
<h2 id="translate-math-packages">Translate math packages</h2>
<p>The LaTeX source might not be written in a way that’s aware of <a href="https://docs.mathjax.org/en/latest/input/tex/index.html">MathJax’s LaTeX support</a>.
So it might use a package that is not supported by MathJax.
To solve this problem, we have to rewrite the math blocks.
There are several places in the pipeline which can be changed to do this and you can combine different transformations together.</p>
<p>Starting from the Pandoc reader, the easiest approach is to translate the bad commands into something supported by MathJax.
Remember it’s only string substitution, so it’s okay to do horrible things like <code>\renewcommand\and[0]{\end{mathpar}\begin{mathpar}}</code>.
I should mention that Pandoc doesn’t handle the star variant of a command well (e.g. <code>\inferrule</code> and <code>\inferrule*</code>).
In LaTeX, <code>*</code> is just an argument to the command, but I didn’t find an easy way to do the <code>if-else</code> branching with Pandoc’s default LaTeX reader, but maybe I’m missing something obvious.</p>
<p>If the above rewriting is not enough to solve your problem, you can use a <a href="https://pandoc.org/lua-filters.html">Pandoc filter</a> to perform whatever transformation you need.
Because of <code>--mathjax</code>, we’ll find raw LaTeX commands as a string at the filter stage and you can rewrite it to a different string.
I wrote some functions to do macro search and replace, which can be found <a href="../assets/macro.lua">here</a> (or see the <a href="https://gist.github.com/crvdgc/efac01603a035b2765974d119dede467">hosted version</a>).</p>
<h2 id="mathjax-performance-issue">MathJax performance issue</h2>
<p>Pandoc prefers building everything as one page, but if there are too many equations, MathJax rendering might take too long and even crash the page with an out-of-memory error.
There are generally two ways to fix this:</p>
<ol type="1">
<li>Use the <a href="https://pandoc.org/MANUAL.html#chunked-html">target of <code>-t chunkedhtml</code></a>.</li>
<li>Pre-render math blocks into SVG or CHTML files.</li>
</ol>
<p>If performance is critical, you can actually do both.
For pre-rendering, examples in <a href="https://github.com/mathjax/MathJax-demos-node/">this repo</a> are very useful.
One note on CHTML pre-rendering, the CSS is necessary and it’s determined by what’s already rendered.
So you need to render everything, then generate the CSS, and <em>compose</em> it with the Pandoc output header.
The <code>tex2chtml-page</code> script won’t change the header if there’s already one.</p>
<h2 id="links-in-general-and-links-in-math-blocks">Links in general and links in math blocks</h2>
<p>In LaTeX, links can be added with <code>\label</code>-<code>\ref</code>, or <code>\hypertarget</code>-<code>\hyperlink</code>.
Pandoc generally translates them into HTML element identifiers and anchors respectively.
When this fails, e.g. <code>\label</code> commands inside figure captions are not translated, you can use a filter to manually parse out the label and add it as an identifier to the element.</p>
<p>In math blocks, the raw LaTeX commands are preserved, so we need to rewrite the string to a format such that <em>after the MathJax rendering</em>, an identifier or an anchor is produced.
For identifiers, <code>\label</code> or <code>\hypertarget</code> can be substituted by MathJax’s <code>\cssId</code>.
For anchors, <code>\ref</code> or <code>\hyperlink</code> can be substituted by <code>\href</code>.
More can be found <a href="https://docs.mathjax.org/en/latest/input/tex/extensions/html.html">in MathJax’s documentation</a>.</p>
<p>There’s a complex interaction between the <code>chunkedhtml</code> target and links.
Pandoc will try to rewrite the links so that they’ll point to the correct chunked page.
However, this doesn’t include identifiers and anchors in math blocks (remember they are only produced after MathJax rendering).
So you’ll need to correct them yourself.
I achieved this by collecting all identifiers and then patching each one to the correct link format of <code>page#id</code>.
See <a href="https://github.com/yuxiliu-arm/herdtools7/blob/110e001c748379ce2325d8523e4357e061894125/asllib/doc/filters/aslref.lua#L274-L322">this example</a>.</p>
<h2 id="anchors-for-section-titles-navigation-sidebar-and-other-goodies">Anchors for section titles, navigation sidebar, and other goodies</h2>
<p>Pandoc’s documentation itself has many additional features.
The implementation can be found at <a href="https://github.com/jgm/pandoc-website">Panodc’s website source code</a>.</p>

        </div>
        </div>
        <div id="footer">
          <div class="inside">
            <p>
              Feed: <a href="../atom.xml">Atom</a>/<a href="../rss.xml">RSS</a>
              Site proudly generated by <a href="http://jaspervdj.be/hakyll">Hakyll</a>.<br />
              Fonts: Serif - <a href="https://github.com/SorkinType/Merriweather">Merriweather</a>, Monospace - <a href="https://github.com/tonsky/FiraCode">FiraCode</a><br />
              Theme adpated from The Professional designed by <a href="http://twitter.com/katychuang">Dr. Kat</a>.<br />
              Original theme showcased in the <a href="http://katychuang.com/hakyll-cssgarden/gallery/">Hakyll-CSSGarden</a>.
            </p>
            <p class="copyright">
                All contents are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
            </p>
          </div>
        </div>
    </body>
</html>

<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="author" content="ubikium" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Cache-Control" content="max-age=86400, must-revalidate" />
        <title>Tips and Reviews of The Little Prover</title>
        <link rel="stylesheet" type="text/css" title="hakyll_theme" href="../css/theprofessional.css" />
        <link rel="stylesheet" type="text/css" title-"syntax_highlighting" href="../css/monokai.css">
        <script src="../assets/highlight.min.js"></script>
        <script type="text/javascript" charset="UTF-8" src="../assets/lean.min.js"></script>
        <script type="text/javascript">hljs.highlightAll();</script>
    </head>
    <body>
      <div class="highbar">&nbsp;</div>
        <div id="header">
          <div class="box">
            <div id="logo" class="name">
              <h2><a href="../">Ubikium</a></h2>
            </div>
            <div id="navigation" class="pageslinks">
              <nav class="menuNav">
                <div class="menuItems">
                <a href="../" class="portfolio/tips-and-reviews-of-the-little-prover.md">Home</a>
                <a href="../about.html" class="portfolio/tips-and-reviews-of-the-little-prover.md">About</a>
                <a href="../archive.html" class="portfolio/tips-and-reviews-of-the-little-prover.md">Portfolio</a>
                </div>
              </nav>
            </div>
        </div>
        </div>
        <div class="container-gallery">
        <div id="content" class="inside">
            <div class="info">
    
      <h1>Tips and Reviews of <i>The Little Prover</i></h1>
    
    Posted on 2023-08-24
    
        by ubikium
    
</div>
<hr>

<p>Similar to <a href="https://ubikium.gitlab.io/portfolio/tips-and-reviews-of-the-little-typer.html">my last post</a> which gives some tips and reviews of <a href="https://thelittletyper.com/"><em>The Little Typer</em></a>, this post will do the same thing, but for <a href="https://the-little-prover.github.io/"><em>The Little Prover</em></a>.</p>
<h2 id="who-is-this-book-for">Who is this book for?</h2>
<p>This book teaches theorem proving over recursive functions using induction.</p>
<p>Suppose you want to prove a theorem related to a function.
A sensible approach is to use a set of rules to rewrite the theorem until we are convinced that it’s true (i.e. it’s rewritten to <code>'t</code>).</p>
<p>The rules are either built-in (e.g. for all <code>a</code>, <code>(equal a a)</code> is true), or they are true by definition (e.g. replace a function application with its substituted body).</p>
<p>However, these are not enough if the function is recursive.
This is because we want to prove that the theorem is true for every possible argument of that function.
For recursive functions, the theorem’s form can be different for different arguments and the number of such forms can go to infinity.
In such cases, we can instead prove by induction.
This requires us to prove that:</p>
<ol type="1">
<li>The function is total.</li>
<li>The theorem is true for base cases.</li>
<li>The theorem is true for recursive cases, given the induction hypotheses.</li>
</ol>
<p>This book will walk you through the whole process for several functions and theorems (literally step-by-step as we’ll see later).
So I think it is especially useful for those who are already motivated for theorem proving, but would want to peek under the hood.</p>
<p>This will not necessarily help you understand the details of other proof assistants, but by going into the details of small examples, you will actually get the general idea.
I think this will be the long lasting benefits you’ll get from reading the book.</p>
<h2 id="set-up-j-bob">Set up J-Bob</h2>
<p>The book’s proof assistant, J-Bob, will check your proof as you follow the examples.
Using J-Bob is quite different from using the Pie language from <em>The Little Typer</em>.
The target language (i.e. the language used to define functions and theorems) is embedded in Scheme as atom lists.
So there are no functionalities like going to the definition, finding all the references, checking the arity and so on.
Instead we will use functions to rewrite the atom list and J-Bob will tell you the result.</p>
<p>To install J-Bob, clone <a href="https://github.com/the-little-prover/j-bob">the repository</a>.
Inside <code>scheme/</code> you’ll find <code>j-bob-lang.scm</code> which defines the target language, <code>j-bob.scm</code> which is the complete implementation of the proof assistant, and <code>little-prover.scm</code> which contains all the examples from the book.</p>
<p>To use J-Bob in DrRacket, you need to set the language to Scheme following the repository’s README instruction.
To check it’s working, use this example from the README:</p>
<pre class="scheme"><code>;; Load the J-Bob language:
(load &quot;j-bob-lang.scm&quot;)
;; Load J-Bob, our little proof assistant:
(load &quot;j-bob.scm&quot;)
;; Load the transcript of all proofs in the book:
(load &quot;little-prover.scm&quot;)
;; Run every proof in the book, up to and including the proof of align/align:
(dethm.align/align)</code></pre>
<p>When you run this file, J-Bob should run every proof in the book and output a list of definitions and theorems.</p>
<h2 id="use-j-bob">Use J-Bob</h2>
<p>Most of the time, what you want is these two lines at the start.</p>
<pre><code>;; Load the J-Bob language:
(load &quot;j-bob-lang.scm&quot;)
;; Load J-Bob, our little proof assistant:
(load &quot;j-bob.scm&quot;)</code></pre>
<p>Which will make the following functions available:</p>
<ol type="1">
<li><code>J-Bob/step</code></li>
<li><code>J-Bob/prove</code></li>
<li><code>J-Bob/define</code></li>
</ol>
<p>And also <code>prelude</code>, which is a list of built-in definitions and theorems.
To learn their usages, at one point in the book, you’ll be directed to the Recess chapter.
However, <code>J-Bob/prove</code> is almost always what you want to use.</p>
<p>To add a definition or theorem, first we need to create a template:</p>
<pre class="scheme"><code>(defun function-name ()
  (J-Bob/prove
   (proven-stuff)
   '(
     ((dethm theorem/name (args...)
             (theorem-body args))
      (induction-scheme) ; or just nil
      ... ; many lines of ((path) (rewritting-rule))
      )
     )))</code></pre>
<p><code>proven-stuff</code> is a list of definitions of functions that are proven to be total and theorems that are proven to be true.
<code>prelude</code> is such an example.
After you’ve proven the current theorem (running <code>(function-name)</code> yields <code>'t</code>), you can replace <code>J-Bob/prove</code> with <code>J-Bob/define</code> to add the function definitions and theorems to that list.
After <code>J-Bob/define</code>, running <code>(function-name)</code> will print a new list of definitions and theorems, including those you just defined.
Then when proving the next theorem, you can supply <code>(function-name)</code> as the first argument of <code>J-Bob/prove</code> and therefore grow your list step by step.</p>
<p>Defining a function is similar, but instead of an induction scheme, you’d supply a measure and the rewriting steps of its totality proof, if necessary.</p>
<p>Note that if you use <code>J-Bob/define</code> without finishing a proof (including the totality proof when defining a function), the new <code>(function-name)</code> will not error, but neither will it include anything that’s not proven.
You need to be careful.
It’s best to use <code>J-Bob/define</code> only after you’ve proven all theorems.</p>
<p>Take a look at <code>little-prover.scm</code> for some examples.</p>
<p>The next step is to write the proof.
Running <code>(function-name)</code> (i.e. hit <code>Run</code> and type <code>(function-name)</code> and enter) should print out the current term.
Then you’d extend the rewriting steps with a new step like <code>((E E) (size/cdr ys))</code>, where the first component is the path to the term to rewrite, and the second component is the rewriting rule supplied with all arguments.</p>
<p>Note that to replace a function call with its definition, the rewriting rule is just the function’s name and arguments.
For example, if we have <code>(defun f (xs) (cons 'a xs))</code>, then to rewrite <code>(f '(b))</code> to <code>(cons 'a '(b))</code>, the rewriting rule should be <code>(f '(b))</code>.</p>
<p>Another important note on the output of <code>J-Bob/prove</code>: if a line of proof is wrong, it has two ways to inform you: either the term is not changed (usually due of a wrong path), or the output is <code>'nil</code> (usually when some function is applied to a wrong number of arguments).
So it’s best to compare the terms before and after each step to make sure that some progress is actually made.</p>
<h2 id="tips-to-streamline-the-proving-process-a-bit">Tips to streamline the proving process (a bit)</h2>
<p>Apart from lacking many language features as mentioned above, J-Bob also has zero automation<a href="#fn1" class="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a>.
Therefore, you need to tell it precisely which term to rewrite and how.
This means to write out the complete path to the term all of the arguments of the rewriting rule, even those not present in the result (which are used internally for equality checking).</p>
<p>It’s the price to pay for an impressively small theorem prover, but these limitations at the same time make proof writing and debugging with J-Bob painful.
Here are some tricks that I used to make the process a little bit easier.</p>
<h3 id="file-organization">File Organization</h3>
<p>My file for each chapter is organized as follows:</p>
<pre class="scheme"><code>(defun f1 ()
  (J-Bob/define
    (prelude)
    '(...)
  ))

(defun f8 ()
  (J-Bob/prove
    (f1)
    '(...)
  ))
(f8)</code></pre>
<p>When proving the theorem from Frame 8 (thus the name <code>f8</code>), I would add a call of <code>(f8)</code> at the end of the definition.
When running the definition, the state of the term is directly printed out.</p>
<h3 id="indent-the-definition-and-the-output">Indent the definition and the output</h3>
<p>Since the proof so often needs you to find the path to a term, it’s very important to indent the definitions and the output so that you can easily navigate around.</p>
<p>Use <code>Racket</code> - <code>Indent all</code> from the menu to indent your definitions (default keybinding <code>Cmd+i</code>).</p>
<p>The output style can be configured through <code>Language</code> - <code>Choose language...</code> and there you can see the language specific output configurations.</p>
<p>The default <code>Output Syntax</code> of <code>write</code> and <code>Insert newlines in printed values</code> are sane choices.
However, I find that the pretty printing seems to assume that the font size is 12.
Using a larger font size will cause the text to overflow to the next line instead of compressing the pretty printing box.
At the time of writing, I didn’t find a bug report or a workaround for this issue, so I just accepted it as a fact of life.
Luckily, the book is short enough such that I still got plenty of eyesight left after finishing it, which is more than what I could have hoped for.</p>
<h3 id="keybindings-to-the-rescue">Keybindings to the rescue</h3>
<p>I adapted the <a href="https://docs.racket-lang.org/drracket/Keyboard_Shortcuts.html#%28part._defining-shortcuts%29">keybinding snippet</a> from the DrRacket reference to bind <code>F4</code> to do three things: <code>Reindent all</code>, <code>Run</code>, and keep the focus at the definition editor.</p>
<pre class="scheme"><code>#lang s-exp framework/keybinding-lang

(require drracket/tool-lib)
(require racket/gui/base)
(module test racket/base)

(define (call-menu menu-item)
  (λ (ed evt)
    (define canvas (send ed get-canvas))
    (when canvas
      (define menu-bar (find-menu-bar canvas))
      (when menu-bar
        (define item (find-item menu-bar menu-item))
        (when item
          (define menu-evt
            (new control-event%
                 [event-type 'menu]
                 [time-stamp
                  (send evt get-time-stamp)]))
          (send item command menu-evt))))))

(define/contract (find-menu-bar c)
  (-&gt; (is-a?/c area&lt;%&gt;) (or/c #f (is-a?/c menu-bar%)))
  (let loop ([c c])
    (cond
      [(is-a? c frame%) (send c get-menu-bar)]
      [(is-a? c area&lt;%&gt;) (loop (send c get-parent))]
      [else #f])))

(define/contract (find-item menu-bar label)
  (-&gt; (is-a?/c menu-bar%)
      string?
      (or/c (is-a?/c selectable-menu-item&lt;%&gt;) #f))
  (let loop ([o menu-bar])
    (cond
      [(is-a? o selectable-menu-item&lt;%&gt;)
       (and (equal? (send o get-plain-label) label)
            o)]
      [(is-a? o menu-item-container&lt;%&gt;)
       (for/or ([i (in-list (send o get-items))])
         (loop i))]
      [else #f])))

(keybinding &quot;f4&quot;
            (λ (ed evt)
              ((call-menu &quot;Reindent All&quot;) ed evt)
              ((call-menu &quot;Run&quot;) ed evt)
              (send (send ed get-canvas) focus)))</code></pre>
<p>To use it, save the definition as a file and load it through the menu item <code>Edit</code> - <code>Keybindings</code> - <code>Add User-defined Keybindings...</code>.
It’s also available as <a href="https://gist.github.com/crvdgc/6ced0f829f2f58efbc02fcee6acd27fc">a gist</a>.</p>
<p>With this keybinding, I can constantly edit my proof and hit <code>F4</code> to see the effect, which almost gives an interactive theorem proving experience.</p>
<h2 id="final-thoughts">Final thoughts</h2>
<p>I really appreciate the effort to keep the language and theorem prover as small as possible, but the lack of some common constructs makes the proving process not so intuitive.
For example, <code>A and B</code> are encoded as <code>(if A (if B 't 'nil) 'nil)</code> which results in many routines to massage the term into the right shape.
Maybe I’m not enlightened enough to see the value of “wax on wax off”, but these routines are more artifacts than practices for me and I think they are rare (or at least easily automated) in practice.</p>
<p>On the other hand, the price we paid gives us a really small language and theorem prover.
After a proof is finished, you can confidently say that you know what’s happening at every single step and there’s literally no magic happening at all.</p>
<p>At the end of the day, being able to do inductive proofs with so little help is really something to be proud of.
See how the confidence grows, when the mystery of theorem proving is unveiled.
I think this book is a good example of going to the details but ended up grasping the general idea.</p>
<p>It’s hard work, but I enjoyed the journey.
If only there were a follow-up of both <em>The Little Typer</em> and <em>The Little Prover</em>…or are you ready to dive in to the boundless space of real life dependant type theorem proving?
Happy hacking and see you on the other side.</p>
<section id="footnotes" class="footnotes footnotes-end-of-document" role="doc-endnotes">
<hr />
<ol>
<li id="fn1"><p>Boyer and J Moore, the namesakes of J-Bob, in contrast, are famous for their <strong>automated</strong> theorem proving systems.<a href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>

        </div>
        </div>
        <div id="footer">
          <div class="inside">
            <p>
              Feed: <a href="../atom.xml">Atom</a>/<a href="../rss.xml">RSS</a>
              Site proudly generated by <a href="http://jaspervdj.be/hakyll">Hakyll</a>.<br />
              Fonts: Serif - <a href="https://github.com/SorkinType/Merriweather">Merriweather</a>, Monospace - <a href="https://github.com/tonsky/FiraCode">FiraCode</a><br />
              Theme adpated from The Professional designed by <a href="http://twitter.com/katychuang">Dr. Kat</a>.<br />
              Original theme showcased in the <a href="http://katychuang.com/hakyll-cssgarden/gallery/">Hakyll-CSSGarden</a>.
            </p>
            <p class="copyright">
                All contents are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
            </p>
          </div>
        </div>
    </body>
</html>

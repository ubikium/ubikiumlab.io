<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="author" content="ubikium" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Cache-Control" content="max-age=86400, must-revalidate" />
        <title>(((Wait a moment .) .) .) - Composing Functions with Multiple Arguments</title>
        <link rel="stylesheet" type="text/css" title="hakyll_theme" href="../css/theprofessional.css" />
        <link rel="stylesheet" type="text/css" title-"syntax_highlighting" href="../css/monokai.css">
        <script src="../assets/highlight.min.js"></script>
        <script type="text/javascript" charset="UTF-8" src="../assets/lean.min.js"></script>
        <script type="text/javascript">hljs.highlightAll();</script>
    </head>
    <body>
      <div class="highbar">&nbsp;</div>
        <div id="header">
          <div class="box">
            <div id="logo" class="name">
              <h2><a href="../">Ubikium</a></h2>
            </div>
            <div id="navigation" class="pageslinks">
              <nav class="menuNav">
                <div class="menuItems">
                <a href="../" class="portfolio/wait-a-moment.md">Home</a>
                <a href="../about.html" class="portfolio/wait-a-moment.md">About</a>
                <a href="../archive.html" class="portfolio/wait-a-moment.md">Portfolio</a>
                </div>
              </nav>
            </div>
        </div>
        </div>
        <div class="container-gallery">
        <div id="content" class="inside">
            <div class="info">
    
      <h1>(((Wait a moment .) .) .) - Composing Functions with Multiple Arguments</h1>
    
    Posted on 2021-03-13
    
        by ubikium
    
</div>
<hr>

<h2 id="panel-1-introduction">Panel 1: Introduction</h2>
<p>I saw this <a href="https://www.reddit.com/r/haskell/comments/m06fdx/pointfree/">meme post</a> in r/haskell about point-free Haskell the other day. It’s meant to be sarcastic, I know, but there are actually some good ideas in it. So I might as well write a blog.</p>
<figure>
<img src="../images/point-free.webp" alt="Point-free meme" />
<figcaption aria-hidden="true">Point-free meme</figcaption>
</figure>
<p>Per the meme, there are four ways to define <code>mapM</code> in terms of <code>sequence</code> and <code>fmap</code>. But actually, you only need to understand their definitions for Panel 1. All the rest can be derived from the first definition, each achieving the goal of point-free to some extent. For our purpose, let’s assume a function <code>composed :: a -&gt; b -&gt; d</code> can be defined in terms of <code>f :: a -&gt; b -&gt; c</code> and <code>g :: c -&gt; d</code>. If we explicitly write out the arguments, then we get the first panel:</p>
<pre class="haskell"><code>composed :: a -&gt; b      -&gt; d
f        :: a -&gt; b -&gt; c
g        ::           c -&gt; d

composed a b = g (f a b)</code></pre>
<p>So our goal can be restated as: to compose <code>f</code> and <code>g</code>, we first accept two arguments, then pass them to <code>f</code>, and finally forward the result to <code>g</code>. Or we can say, we want to <em>wait for two arguments</em> before forwarding the result.</p>
<p>A more familiar case is when we just want to <em>wait for one argument</em>.</p>
<pre class="haskell"><code>composed' :: a      -&gt; d
f'        :: a -&gt; c
g'        ::      c -&gt; d

composed' a = g' (f' a)</code></pre>
<p>In this case, we have the usual function composition operator</p>
<pre class="haskell"><code>(.) :: (b -&gt; c) -&gt; (a -&gt; b) -&gt; (a -&gt; c)
(g . f) a = g (f a)</code></pre>
<p>Then it’s easy to see how we can make <code>composed'</code> point-free:</p>
<pre class="haskell"><code>composed' a = (g' . f') a
composed'   =  g' . f'        -- η reduction</code></pre>
<p>The problem, of course, is how to find a similar operator to wait for two arguments, or in general, wait for <span class="math inline"><em>n</em></span> arguments.</p>
<h2 id="panel-2-curry-or-uncurry-that-is-the-question">Panel 2: <code>curry</code> or <code>uncurry</code>, that is the question!</h2>
<pre class="haskell"><code>composed = curry (g . uncurry f)</code></pre>
<p>Panel 2 uses <code>curry</code> and <code>uncurry</code> to bundle the two arguments together into a pair, then wait for the bundle with <code>(.)</code>. When passing the bundle to <code>f</code>, we need to unbundle it.</p>
<p>Recall the definition of <code>curry</code> and <code>uncurry</code>:</p>
<pre class="haskell"><code>curry   :: ((a, b) -&gt; c) -&gt; (a -&gt; b -&gt; c)
uncurry :: (a -&gt; b -&gt; c) -&gt; ((a, b) -&gt; c)</code></pre>
<p>Then we can derive Panel 2:</p>
<pre class="haskell"><code>                   f  :: a -&gt; b -&gt; c
           uncurry f  :: (a, b) -&gt; c
       g              ::           c -&gt; d
       g . uncurry f  :: (a, b)      -&gt; d
curry (g . uncurry f) :: a -&gt; b      -&gt; d</code></pre>
<p>This works fine for waiting for two arguments, but it’s not a good example for point-free style or abstraction because it</p>
<ol type="1">
<li>doesn’t separate the composition operator with the actual functions to be composed, they are instead intertwined together.</li>
<li>doesn’t generalize well if we want to wait for three or more arguments, we need to implement <code>curry3</code> and <code>uncurry3</code>.</li>
</ol>
<p>Therefore, we can’t read the intention of the code well. It doesn’t directly express the idea of <em>waiting for two arguments</em>.</p>
<h2 id="panel-3-birds-of-a-feather">Panel 3: Birds of a feather</h2>
<pre class="haskell"><code>composed = ((.).(.)) g f</code></pre>
<p>The third panel seems promising, because it separates the operator and the functions, but what on earth is <code>(.).(.)</code>? (spoiler: it’s not on earth!)</p>
<p>I tried very hard to understand it the first time I met it. My current best explanation is as follows:</p>
<p>Recall the partial application of <code>(.)</code>, we have <code>(.) f ≡ (f .)</code>(<span class="math inline">*</span>).</p>
<p>At first, we have <code>((.).(.)) g f</code>, so when <code>(.).(.)</code> gets the first argument <code>g</code>, what does it do?</p>
<p>Since it’s a composition of two functions, it will pass <code>g</code> to the latter function and forward the result to the former function.</p>
<p>That is <code>(secondF . firstF) g ≡ secondF (firstF g)</code>. Here we take both <code>secondF</code> and <code>firstF</code> to be <code>(.)</code>. Then</p>
<pre class="haskell"><code>(secondF . firstF) g ≡ secondF (firstF g)
                     ≡ secondF ((.)    g)
                     ≡ secondF (g .)      -- see (*) above
                     ≡ (.)     (g .)
                     ≡ ((g .) .)          -- see (*) above</code></pre>
<p>Then we apply it to the second argument <code>f</code>, we get <code>((.).(.)) g f ≡ (g .) . f</code>.</p>
<p>How can we understand <code>(g .) . f</code> then?</p>
<p>Well, it’s a function composition. So when the first argument <code>a</code> comes, it will be passed to <code>f</code>, and the result will be forward to <code>(g .)</code>.</p>
<pre class="haskell"><code>(g .) . f $ a ≡ (g .) (f a)
              ≡  g .  (f a)</code></pre>
<p>Then the second argument <code>b</code> comes, what will it do? Oh, it is again a function composition, so we pass <code>b</code> to the second function <code>(f a)</code>, and then forward the result to <code>g</code>.</p>
<pre class="haskell"><code>g . (f a) $ b ≡ g ((f a) b)
              ≡ g ( f a  b)</code></pre>
<p>We achieved our goal.</p>
<p>This can also be understood through the type signature. After the first argument <code>a</code> comes to <code>(g .) . f</code>, it will be passed to <code>f :: a -&gt; b -&gt; c</code>, resulting in <code>f a :: b -&gt; c</code>. Then we can just compose it with <code>g :: c -&gt; d</code> with the usual <code>(.)</code>.</p>
<p>So far, we have established that <code>(g .) . f</code> will wait for two arguments. Also, we know <code>g . f</code> will wait for one argument. Actually, these are special cases for a general pattern:</p>
<pre class="haskell"><code>g . f         -- wait for 1 more argument
(g .) . f     -- wait for 2 more arguments
((g .) .) . f -- wait for 3 more arguments</code></pre>
<p>Or equivalently, the operator style:</p>
<pre class="haskell"><code>(.)         -- wait for 1 more argument
(.).(.)     -- wait for 2 more arguments
(.).(.).(.) -- wait for 3 more arguments</code></pre>
<p>The type signatures will tell us what they do:</p>
<pre class="haskell"><code>(.)         :: (d -&gt; e) -&gt; (          c -&gt; d) -&gt; (          c -&gt; e)
(.).(.)     :: (d -&gt; e) -&gt; (     b -&gt; c -&gt; d) -&gt; (     b -&gt; c -&gt; e)
(.).(.).(.) :: (d -&gt; e) -&gt; (a -&gt; b -&gt; c -&gt; d) -&gt; (a -&gt; b -&gt; c -&gt; e)</code></pre>
<p>Although presented in a meme, these are actually quite useful and common higher-order operators. Once you know them, they’re pretty easy to read, remember, and write.</p>
<p><code>(.).(.)</code> is often presented as the first non-trivial example in these operators. It’s called a <em>blackbird</em> operator in <a href="https://www.youtube.com/watch?v=seVSlKazsNk&amp;t=674s">Amar Shah’s talk about point-free</a>, a <em>dot</em> in <a href="https://wiki.haskell.org/Pointfree">the Haskell wiki for point-free</a>.</p>
<p>This family of operators is also an example for Semantic Editor Combinators in <a href="https://www.youtube.com/watch?v=cefnmjtAolY&amp;t=584s">Edward Kmett’s talk about lenses</a>.</p>
<h2 id="panel-4-one-of-them-is-not-like-the-others">Panel 4: One of them is not like the others</h2>
<pre class="haskell"><code>composed = fmap fmap fmap g f</code></pre>
<p>In Panel 3, there are three points. BUt i thOught wE weRE DOing poINT-FREe PROGrAmMINg! So for Panel 4, we have three <code>fmap</code> and zero points. A reasonable guess would be that <code>fmap</code> is secretly <code>(.)</code>. But how?</p>
<p>Recall their definitions:</p>
<pre class="haskell"><code>(.)  ::              (b -&gt; c) -&gt; (a -&gt; b) -&gt; (a -&gt; c)
fmap :: Functor f =&gt; (b -&gt; c) -&gt; (   f b) -&gt; (   f c)</code></pre>
<p>So here, we are instantiating the functor <code>f</code> to be <code>(a -&gt;)</code>. That is, we partially apply a fixed type <code>a</code> to <code>(-&gt;)</code>, and change the return type variable, resulting in a (covariant) functor.</p>
<p>This functor is known as the <em>Reader</em> functor. We can explicitly define it as <code>data Reader a x = Reader { runReader :: a -&gt; x }</code>. But in Haskell, we can just write <code>((-&gt;) a)</code>.</p>
<p>You can verify that its functor instance must be</p>
<pre class="haskell"><code>instance Functor ((-&gt;) a) where
    fmap = (.)</code></pre>
<p>You can read more about the Reader functor in <a href="https://bartoszmilewski.com/2015/01/20/functors/">this post from Bartosz’s blog</a>.</p>
<p>Back to the meme, if we replace all <code>(.)</code> with <code>fmap</code> in Panel 3, then we get Panel 4.</p>
<p>It’s correct, but really confusing. Without looking at the signature, one can hardly imagine which instance is <code>fmap</code> using, let alone the intention of three <code>fmap</code> being together (the original meme also use <code>fmap</code> as an argument, just to be fun!).</p>
<p>There is, however, a chance to salvage something from Panel 4. Let’s think <code>fmap fmap fmap</code> in terms of itself, instead of being just <code>(.)</code> in disguise.</p>
<p>One way to type check <code>fmap fmap fmap</code> is to let the first <code>fmap</code> be instantiated to the Reader functor <code>fmap</code>, i.e. <code>(.)</code>. Then <code>fmap fmap fmap</code> becomes <code>(.) fmap fmap ≡ fmap . fmap</code>.</p>
<p>The type signature can be derived as</p>
<pre class="haskell"><code>fmap :: Functor f1 =&gt; (a -&gt; b) -&gt; (f1 a -&gt; f1 b)
fmap :: Functor f2 =&gt;             (c    -&gt; d   ) -&gt; (f2 c -&gt; f2 d)</code></pre>
<p>We can take <code>c</code> to be <code>f1 a</code> and <code>d</code> to be <code>f1 b</code>. Therefore</p>
<pre class="haskell"><code>fmap        :: Functor f1               =&gt; (a -&gt; b) -&gt; (f1 a -&gt; f1 b)
fmap        :: Functor f1               =&gt;             (f1 a -&gt; f1 b) -&gt; f2 (f1 a) -&gt; f2 (f1 b)
fmap . fmap :: (Functor f1, Functor f2) =&gt; (a -&gt; b)                   -&gt; f2 (f1 a) -&gt; f2 (f1 b)</code></pre>
<p>So by combining two <code>fmap</code>, we get one functor level <em>deeper</em> into the structure. This is another example of Semantic Editor Combinators also mentioned in <a href="https://www.youtube.com/watch?v=cefnmjtAolY&amp;t=584s">the Kmett talk</a>. The general case is:</p>
<pre class="haskell"><code>fmap               :: (Functor f1)                         =&gt; (a -&gt; b) -&gt; f3 a           -&gt; f3 b
fmap . fmap        :: (Functor f1, Functor f2)             =&gt; (a -&gt; b) -&gt; f3 (f2 a)      -&gt; f3 (f2 b)
fmap . fmap . fmap :: (Functor f1, Functor f2, Functor f3) =&gt; (a -&gt; b) -&gt; f3 (f2 (f1 a)) -&gt; f3 (f2 (f1 b))</code></pre>
<p>We go one functor level deeper for each <code>fmap</code> composed. Keep digging this way, then you might find some lens for yourself!</p>
<h2 id="conclusion-whats-the-point-of-point-free-anyway">Conclusion: What’s the point of point-free anyway?</h2>
<p>The goal of the point-free style is the same as all other forms of abstraction: to express your intention in a <em>concise</em> (to write), <em>clear</em> (to read), and <em>general</em> (to change) way. All fore-mentioned methods can be summarized as follows:</p>
<div class="table-wrapper">
<table>
<thead>
<tr class="header">
<th style="text-align: left;">Definition</th>
<th style="text-align: center;">Point-free</th>
<th style="text-align: center;">Concise</th>
<th style="text-align: center;">Clear</th>
<th style="text-align: center;">General</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><code>composed a b = g (f a b)</code></td>
<td style="text-align: center;">X</td>
<td style="text-align: center;">X</td>
<td style="text-align: center;">O</td>
<td style="text-align: center;">X</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>composed = curry (g . uncurry f)</code></td>
<td style="text-align: center;">O</td>
<td style="text-align: center;">X</td>
<td style="text-align: center;">X</td>
<td style="text-align: center;">X</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>composed = ((.).(.)) g f</code></td>
<td style="text-align: center;">O</td>
<td style="text-align: center;">O</td>
<td style="text-align: center;">O*</td>
<td style="text-align: center;">O*</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>composed = fmap fmap fmap g f</code></td>
<td style="text-align: center;">O</td>
<td style="text-align: center;">O</td>
<td style="text-align: center;">X</td>
<td style="text-align: center;">O*</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>composed = (fmap . fmap) g f</code></td>
<td style="text-align: center;">O</td>
<td style="text-align: center;">O</td>
<td style="text-align: center;">O*</td>
<td style="text-align: center;">O*</td>
</tr>
</tbody>
</table>
</div>
<p>(*): some experience required</p>
<p>Conclusions:</p>
<ol type="1">
<li>If you need clarity and don’t want to surprise others, go for Panel 1, write all arguments explicitly: <code>composed a b = g (f a b)</code>.</li>
<li>Not every point-free change is guaranteed to be an improvement. You should avoid Panel 2 style (point-free for the sake of point-free) as much as possible. A point-free change should have a strong semantic encoding.</li>
<li>For Panel 3 and modified Panel 4, we can see the power of good point-free usage. We find a pattern (<code>(.).(.)</code> and <code>fmap . fmap</code>) with almost all of the advantages. However, they might be difficult to understand for those who are unfamiliar. And it takes a great deal of efforts to find a new one. Therefore, it’s good for in-house use.</li>
<li>For the original Panel 4, try not to hide your intention. Replace some instances of <code>fmap</code> with its concrete instance to avoid repetition and ambiguity (even just for humans).</li>
</ol>
<hr />
<p>Edit notes:</p>
<ul>
<li>The <code>(.).(.)</code> is called a <em>blackbird</em>, a special kind of bird. That is to say, it’s a special case of an operator in a family of operators. Thanks for Jean-Baptiste Mazon to point it out.</li>
</ul>

        </div>
        </div>
        <div id="footer">
          <div class="inside">
            <p>
              Feed: <a href="../atom.xml">Atom</a>/<a href="../rss.xml">RSS</a>
              Site proudly generated by <a href="http://jaspervdj.be/hakyll">Hakyll</a>.<br />
              Fonts: Serif - <a href="https://github.com/SorkinType/Merriweather">Merriweather</a>, Monospace - <a href="https://github.com/tonsky/FiraCode">FiraCode</a><br />
              Theme adpated from The Professional designed by <a href="http://twitter.com/katychuang">Dr. Kat</a>.<br />
              Original theme showcased in the <a href="http://katychuang.com/hakyll-cssgarden/gallery/">Hakyll-CSSGarden</a>.
            </p>
            <p class="copyright">
                All contents are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
            </p>
          </div>
        </div>
    </body>
</html>

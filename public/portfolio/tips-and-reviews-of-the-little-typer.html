<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="author" content="ubikium" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Cache-Control" content="max-age=86400, must-revalidate" />
        <title>Tips and Reviews of The Little Typer</title>
        <link rel="stylesheet" type="text/css" title="hakyll_theme" href="../css/theprofessional.css" />
        <link rel="stylesheet" type="text/css" title-"syntax_highlighting" href="../css/monokai.css">
        <script src="../assets/highlight.min.js"></script>
        <script type="text/javascript" charset="UTF-8" src="../assets/lean.min.js"></script>
        <script type="text/javascript">hljs.highlightAll();</script>
    </head>
    <body>
      <div class="highbar">&nbsp;</div>
        <div id="header">
          <div class="box">
            <div id="logo" class="name">
              <h2><a href="../">Ubikium</a></h2>
            </div>
            <div id="navigation" class="pageslinks">
              <nav class="menuNav">
                <div class="menuItems">
                <a href="../" class="portfolio/tips-and-reviews-of-the-little-typer.md">Home</a>
                <a href="../about.html" class="portfolio/tips-and-reviews-of-the-little-typer.md">About</a>
                <a href="../archive.html" class="portfolio/tips-and-reviews-of-the-little-typer.md">Portfolio</a>
                </div>
              </nav>
            </div>
        </div>
        </div>
        <div class="container-gallery">
        <div id="content" class="inside">
            <div class="info">
    
      <h1>Tips and Reviews of <i>The Little Typer</i></h1>
    
    Posted on 2023-08-19
    
        by ubikium
    
</div>
<hr>

<h2 id="who-is-this-book-for">Who is this book for?</h2>
<p><a href="https://thelittletyper.com/"><em>The Little Typer</em></a> is an introduction to dependent types, but not a textbook.
So you won’t get deep theories, or practical details.
Instead, you’ll get what’s like to “do” dependent types.</p>
<p>The main gain of dependent types, in my opinion, is the ability to encode a useful property into a type.
Then the type checker will help you to construct a program of that type, and at the same time, you get a proof of that property.</p>
<p>In the book, the dialogue will walk you through several examples of this process.
And it’s even better if you try to work out the proof by yourself before reading the text.
In the end, you’ll have a pretty good idea of the workflow and will perhaps be eager to try out on “real world” problems.</p>
<p>So I think this book is best for those who are looking from the outside, or trying to get their very first steps into the field of dependent types.
It’s a system small enough to understand and experiment with freely.
Then you can take what you’ve learned and meet the industrial level systems.
They will feel less magical and more grounded.
Although you might not know the exact mechanism, you’ll find the intention and the general direction very familiar.</p>
<p>It’s not exactly a bad deal, to get the <strong>big</strong> picture out of the <strong>little</strong> typer.</p>
<h2 id="set-up-the-pie">Set up the Pie</h2>
<p>Playing with the <a href="https://github.com/the-little-typer/pie">Pie language</a> is perhaps the most essential part of the learning process, but the set up is not exactly simple, at least not for me.</p>
<p>What the above linked repository contains is the implementation of the Pie language, not code examples used in the book.
As is stated in README, the language is provided as a package to Racket, and it’s already in the Racket package server.
So to use the language, you don’t need to clone or build the repository (it doesn’t build for me).
What you want is to install Racket, then install the <code>pie</code> package.
After that you’re good to go.</p>
<h3 id="install-drracket-from-racket">Install DrRacket from Racket</h3>
<p>The path of least resistance is to use DrRacket.
It’s an editor/IDE that comes with Racket.
To install Racket, head to the <a href="https://download.racket-lang.org">download page</a> and choose your platform.</p>
<h3 id="install-pie">Install Pie</h3>
<p>After Racket is installed, open DrRacket.
Then follow the Installation Instructions from README:</p>
<blockquote>
<p>From DrRacket</p>
<p>Click the “File” menu, and then select “Install Package…”. Type <code>pie</code> in the box, and click the “Install” button.</p>
</blockquote>
<p>which will download and install <code>pie</code>.</p>
<p>To create and run Pie programs, start the file with <code>#lang pie</code>.
You might need to restart DrRacket for the changes to take effect.</p>
<p>To check the installation, put the following into the editor:</p>
<pre class="racket"><code>#lang pie

(claim +
  (→ Nat Nat
    Nat))</code></pre>
<p>when you hover over keywords like <code>claim</code> or <code>Nat</code>, there should be an arrow pointing to it from <code>pie</code> and a floating window saying something like “imported from pie”.</p>
<p>You can also use the REPL (or the “interaction”) to test.
In the menu, find “Racket”, then hit “Run” (or press <code>F5</code>), which should bring up the interaction window.
Type <code>(add1 zero)</code>, then hit enter, it should report <code>(the Nat 1)</code>.</p>
<h3 id="configure-drracket-for-pie-optional">Configure DrRacket for Pie (optional)</h3>
<p>All of the following configurations are optional but useful.</p>
<h4 id="preference">Preference</h4>
<p>Inside the <code>DrRacket</code> - <code>Preferences</code> setting group:</p>
<ol type="1">
<li>Font and colors.
One feature that I find especially useful is <code>Colors</code> - <code>Racket</code>, which allows you to define styles for different component types.
Another one is <code>Colors</code> - <code>Background</code> - <code>Parenthesis color scheme</code>.
You can use e.g. “Shades of grey”, so that different levels of parentheses have different shades of colors.</li>
<li>Background Expansion.
Tick “Enable background expansion”, so that errors are reported as you type.
I also find the “with gold highlighting” option very useful.</li>
</ol>
<h4 id="keybindings">Keybindings</h4>
<p>These are platform specific.
Three especially useful ones are <code>Run</code> (<code>F5</code> by default), <code>Check Syntax</code> (<code>F6</code>), and <code>Reindent All</code> (<code>d:i</code>, which means <code>Cmd+i</code>).</p>
<p>You can show and search existing key bindings from <code>Edit</code> - <code>Keybindings</code> - <code>Show Active Keybindings</code>.</p>
<p>macOS users might want to tick <code>Treat alt key as meta</code> under <code>DrRacket</code> - <code>Preferences</code> - <code>Editing</code> - <code>General Editing</code> for some keybindings to work.</p>
<h4 id="input-fancy-symbols">Input fancy symbols</h4>
<p>The book uses unicode symbols extensively and Pie uses them as well.</p>
<p>It is also possible to use the ASCII version directly, like <code>-&gt;</code> for <code>→</code>, <code>fun</code> for <code>λ</code>, etc.
You can find a complete list of these mappings in <em>The Pie Reference</em>, available <a href="https://docs.racket-lang.org/pie">online</a> or locally from <code>search-help-desk</code> (default <code>F1</code>) and then search for <code>pie</code>.</p>
<p>Alternatively, there’s <a href="https://docs.racket-lang.org/drracket/Keyboard_Shortcuts.html#%28part._.La.Te.X_and_.Te.X_inspired_keybindings%29">the LaTeX approach</a>, which gets tedious after a while.</p>
<p>What I used is to input these symbols via <a href="https://docs.racket-lang.org/drracket/Keyboard_Shortcuts.html#%28part._defining-shortcuts%29">custom keyboard shortcuts</a>.</p>
<p>Create a file with the following content:</p>
<pre class="racket"><code>#lang s-exp framework/keybinding-lang
(keybinding &quot;d:r&quot; (λ (editor evt) (send editor insert &quot;→&quot;)))
(keybinding &quot;d:p&quot; (λ (editor evt) (send editor insert &quot;Π&quot;)))
(keybinding &quot;d:l&quot; (λ (editor evt) (send editor insert &quot;λ&quot;)))
(keybinding &quot;d:s:s&quot; (λ (editor evt) (send editor insert &quot;Σ&quot;)))</code></pre>
<p>From the menu, <code>Edit</code> - <code>Keybindings</code> - <code>Add User-defined Keybindings...</code> and select this file.
Note that DrRacket only loads the keybindings at start.
So after a change to the file or new bindings being added, you need to restart DrRacket for it to take effect.</p>
<p>Then you can use <code>Cmd+r</code> to input right arrow and so on.
Notice I used <code>d:s:s</code> or <code>Cmd+Shift+s</code> for <code>Σ</code> because <code>d:s</code> was used for saving the definition.</p>
<p>Read the documentation and create your own keybindings.
And again, macOS users might want to tick <code>Treat alt key as meta</code> under <code>DrRacket</code> - <code>Preferences</code> - <code>Editing</code> - <code>General Editing</code> for some keybindings to work.</p>
<h2 id="tips-for-middle-to-late-chapters">Tips for middle to late chapters</h2>
<p>If you find any part of the book difficult, one option is to revisit previous materials and do the exercise again.
Apart from this “git gud” advice, I also have an approach that works well for me.
I should highlight that I had some experience with other proof assistant, so it might not be universally applicable.</p>
<p>When a new concept is introduced, I find it useful to think about the motivation.
For example, why is <code>mot</code> needed for induction?
Is it possible to do induction without this argument?
Do we have <code>mot</code> in other proof assistants?
Why or why not?
What’s the minimum information needed to automatically fill out a hypothetical <code>mot</code>?</p>
<p>Answering these questions helped me a lot for later chapters.
For example, figuring out the purpose of <code>mot</code> in the definition of <code>replace</code>.</p>
<p>For the last question specifically, before reading Chapter 9, I recommend <a href="https://ahelwer.ca/post/2022-10-13-little-typer-ch9/">this blog post by Andrew Helwer</a>, which adds a small dialogue before the text to clarify some concepts.
I find it very helpful.</p>
<h2 id="todo-helps-sometimes"><code>TODO</code> helps, sometimes</h2>
<p>In the source file, you can write <code>TODO</code> in place of a term.
DrRacket will give you a list <code>TODO</code>s and for each one, the needed type and the typing environment.
This corresponds to the boxes in the book.
It is called a typed hole <a href="https://wiki.haskell.org/GHC/Typed_holes">in Haskell</a> and Agda.</p>
<p>This feature would have been much more useful if it weren’t for two limitations of Pie:</p>
<ol type="1">
<li>The type checker often cannot determine the type in presence of <code>TODO</code>.</li>
<li>Types are often “elaborated”, that is, with definitions unfolded, making it hard to read.</li>
</ol>
<p>These limitations are understandable since Pie needs to be small and simple.
However, this also makes me appreciate the “delaborator” in Lean and notation printing in Coq more.</p>
<h2 id="learn-by-playing-and-reading-or-thank-you-jonathan-blow">Learn by playing <em>and</em> reading, or thank you Jonathan Blow</h2>
<p>At the end, I want to reflect a bit on the methodology exemplified by this book.</p>
<p>You know that a conventional wisdom of story telling is “show, do no tell”.
Well, this book certainly does not “tell” you what are dependent types, but “shows” you how to do dependent types.
With the provided Pie language, it actually follows a better principle.
I call it “play, and thus show”.</p>
<p>In a talk titled <a href="https://www.youtube.com/watch?v=qWFScmtiC44">Video games and the future of education</a>, Jonathan Blow identifies two ways of learning: by playing with a thing and by reading a book.
Of course, these two methods are not mutually exclusive, rather, they often complement each other.</p>
<p>Playing can give you a lot of knowledge, fast and intuitively, but often implicit and imprecise.
Think about writing a manual to teach people how to ride a bike.
After thousands of pages, it would still be difficult to get the idea through.
Books, on the other side, is very good at conveying precise knowledge, but it can be quite hard to make progress (or even to evaluate the progress already made!).
Jonathan postulates that the ideal form of education should be a combination of both methods.</p>
<p>Well, with the advent of a myriad of interactive textbooks, we can get pretty close to that ideal form:</p>
<ol type="1">
<li><a href="https://softwarefoundations.cis.upenn.edu/"><em>Software Foundations</em></a> comes with exercises to be finished with the help of and checked by the interactive theorem prover Coq</li>
<li><a href="https://www.ma.imperial.ac.uk/~buzzard/xena/natural_number_game/">Natural Number Game</a> by Kevin Buzzard (and <a href="https://adam.math.hhu.de/#/g/hhu-adam/NNG4">the Lean 4 port</a>) teaches the Lean theorem prover with carefully designed levels</li>
<li><a href="https://github.com/IHaskell/learn-you-a-haskell-notebook">An interactive version of <em>Learn You a Haskell for Greater Good!</em></a> teaches you, well, Haskell.
The interactive version was ported by my friend James Brock, a big believer in interactive learning and the <a href="https://www.theatlantic.com/science/archive/2018/04/the-scientific-paper-is-obsolete/556676/">computational essay</a></li>
</ol>
<p>I think <em>The Little Typer</em> along with the Pie language could well be <code>cons</code>ed onto this list of shiny examples.
Anyway, it is a much bigger topic and beyond the scope of this little blog post.</p>
<p>Enjoy your read and play of <em>The Little Typer</em>!</p>

        </div>
        </div>
        <div id="footer">
          <div class="inside">
            <p>
              Feed: <a href="../atom.xml">Atom</a>/<a href="../rss.xml">RSS</a>
              Site proudly generated by <a href="http://jaspervdj.be/hakyll">Hakyll</a>.<br />
              Fonts: Serif - <a href="https://github.com/SorkinType/Merriweather">Merriweather</a>, Monospace - <a href="https://github.com/tonsky/FiraCode">FiraCode</a><br />
              Theme adpated from The Professional designed by <a href="http://twitter.com/katychuang">Dr. Kat</a>.<br />
              Original theme showcased in the <a href="http://katychuang.com/hakyll-cssgarden/gallery/">Hakyll-CSSGarden</a>.
            </p>
            <p class="copyright">
                All contents are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
            </p>
          </div>
        </div>
    </body>
</html>

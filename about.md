---
title: About
---

Ubikium, the essence of Ubik.

Name from Philip K. Dick's novel *Ubik*.

This is a personal blog covering the following topics:

1. Haskell
2. Functional Programming
3. Programming Languages
4. Software Engineering

## Contact Information

E-mail: <a href="mailto: ubikium@gmail.com">ubikium@gmail.com</a>

If you spot an error, or want to send some comments, please create an issue at [the source repository](https://gitlab.com/ubikium/ubikium.gitlab.io).

